from django.contrib import admin
from .models import utilisateur
class UtilisateurAdmin(admin.ModelAdmin):
   list_display   = ('pseudo', 'email', 'walletDevise')
   list_filter    = ('pseudo','walletDevise',)
   ordering       = ('pseudo', )
   search_fields  = ('titre', 'contenu')
admin.site.register(utilisateur,UtilisateurAdmin)
# Register your models here.
