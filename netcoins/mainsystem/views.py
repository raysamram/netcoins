from django.shortcuts import render
from django.http import HttpResponse
from django.core.mail import send_mail
from django.http import HttpResponse, Http404
from mainsystem.models import utilisateur
from .forms import subscribeForm
from django.http import JsonResponse
from django.conf import settings
from django.contrib.auth.tokens import default_token_generator
from django.utils.http import urlsafe_base64_encode
from django.utils.encoding import force_bytes, force_text
import urllib
import urllib.request
import requests
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
import json
from .tokens import account_activation_token
from django.core.mail import EmailMessage
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes

# Create your views here.
def home(request):
    form= subscribeForm(request.POST or None)
    print('VALID : '+str(form.errors))
    if True:
        if form.is_valid():
            recaptcha_response = request.POST.get('g-recaptcha-response')
            data = {
                'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
                'response': recaptcha_response
            }
            r = requests.post('https://www.google.com/recaptcha/api/siteverify', data=data)
            result = r.json()
            print('Hi')
            
            if result['success']:
                print('Success captcha :)')
            else:
                return HttpResponse('Captcha non réussi !')

            prenom=form.cleaned_data['prenom']
            nom=form.cleaned_data['nom']
            email=form.cleaned_data['email']
            pays=form.cleaned_data['pays']
            telephone=form.cleaned_data['telephone']
            adresse=form.cleaned_data['adresse']
            complement_adresse=form.cleaned_data['complement_adresse']
            postal=form.cleaned_data['postal']
            pseudo=form.cleaned_data['pseudo']
            parrain=form.cleaned_data['parrain']
            mdp=form.cleaned_data['mdp']
            if parrain!='':
                s=utilisateur.objects.get(pseudo=parrain)
                ur=utilisateur(prenom=prenom,nom=nom,email=email,parrain=s,pays=pays,telephone=telephone,adresse=adresse,complement_adresse=complement_adresse,postal=postal,password=mdp,walletEuro='0',walletDevise='0',pseudo=pseudo,status='inactif')
                ur.save()
            else:
                ur=utilisateur(prenom=prenom,nom=nom,email=email,pays=pays,telephone=telephone,adresse=adresse,complement_adresse=complement_adresse,postal=postal,password=mdp,walletEuro='0',walletDevise='0',pseudo=pseudo,status='inactif')
                ur.save()
        
        
            mail_subject = 'Activation du compte Netcoins.'
            current_site = get_current_site(request)
            uid = urlsafe_base64_encode(force_bytes(ur.user_id))
            uid=str(uid.decode("utf-8")) 
            token = account_activation_token.make_token(ur)
            activation_link = "{0}/activate/{1}/{2}".format(current_site, uid, token)
            message = 'Bienvenue '+str(ur.pseudo)+',\n <a href="'+str(activation_link)+'">Confirmez votre inscription.'
            to_email = form.cleaned_data.get('email')
            email = EmailMessage(mail_subject, message, to=[to_email])
            email.send()
            return render(request,'index.html',{'success':True,'message':'Inscription réussie. Veuillez vérifier votre boite email pour confirmer.','form':form})
    else:
        return render(request,'index.html',{'problem':True,'message':'Veuillez remplir le formulaire avec des informations valides.','form':form})
    return render(request,'index.html',{'form':form})


def validate_username(request,pseudo):
    data = {
        'is_taken': utilisateur.objects.filter(pseudo=pseudo).exists()
    }
    return JsonResponse(data)

def validate_email(request,email):
    data = {
        'is_taken': utilisateur.objects.filter(email=email).exists()
    }
    return JsonResponse(data)

def validate_telephone(request,telephone):
    data = {
        'is_taken': utilisateur.objects.filter(telephone=telephone).exists()
    }
    return JsonResponse(data)


def activation_view(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = utilisateur.objects.get(user_id=uid)
    except(TypeError, ValueError, OverflowError, utilisateur.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        # activate user and login:
        user.is_active = True
        user.status="actif"
        user.save()
        return render(request,'index.html',{'success':True,'message':'Activation de votre compte réussie. Vous pouvez vous connecter dorénavant.'})
    else:
        return render(request,'index.html',{'problem':True,'message':'Activation de votre compte echouée.'})


def profile(request, pseudo):
    try:
        u=utilisateur.objects.get(pseudo=pseudo)
    except:
        return HttpResponse('Rien trouvé')
    return render(request,'public/profile.html',{'utilisateur':u})