from django.db import models

class utilisateur(models.Model):
    user_id = models.AutoField(primary_key=True)
    prenom = models.CharField(max_length=100)
    nom = models.CharField(max_length=100)
    last_login = models.DateTimeField(auto_now=False, auto_now_add=False,null=True,blank=True)
    email  = models.CharField(max_length=100,unique=True)
    pays   = models.CharField(max_length=100)
    is_active = models.BooleanField(default=False)
    status = models.CharField(max_length=100,default="inactif")
    telephone = models.BigIntegerField(unique=True)
    adresse  = models.CharField(max_length=100)
    complement_adresse  = models.CharField(max_length=100)
    postal= models.CharField(max_length=10)
    pseudo = models.CharField(max_length=100, unique=True)
    parrain = models.ForeignKey('self',related_name='pseudonyme',on_delete=models.CASCADE,null=True,blank=True)
    password  = models.CharField(max_length=100)
    walletEuro = models.FloatField()
    walletDevise  = models.FloatField()
    
    class Meta:
        verbose_name = "Utilisateur"
        ordering = ['pseudo']
    def __str__(self):              # __unicode__ on Python 2
        return 'prenom:'+str(self.prenom)+'|nom:'+str(self.nom)+'|adresse:'+str(self.adresse)