from django import forms
from .models import utilisateur

class subscribeForm(forms.Form):
    prenom = forms.CharField(max_length=100, required=True)
    nom = forms.CharField(max_length=100, required=True)
    email  = forms.EmailField(label="Votre adresse e-mail", required=True)
    pays   = forms.CharField(max_length=100, required=True)
    telephone = forms.IntegerField(required=True)
    adresse  = forms.CharField(max_length=100, required=True)
    complement_adresse  = forms.CharField(max_length=100,required=False) 
    postal= forms.IntegerField(required=True)
    pseudo = forms.CharField(max_length=100, required=True)
    parrain = forms.CharField(max_length=100,required=False)
    mdp  = forms.CharField(widget=forms.PasswordInput,max_length=100, required=True)
    class Meta:
        model=utilisateur