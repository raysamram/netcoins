from django.urls import path, include
from . import views
from django.conf.urls import url
urlpatterns = [
    path('',views.home,name='home'),
    path('profile/<pseudo>', views.profile),
    path('ajax/validate_username/<pseudo>', views.validate_username, name='validate_username'),
    path('ajax/validate_email/<email>', views.validate_email, name='validate_email'),
    path('ajax/validate_telephone/<telephone>', views.validate_telephone, name='validate_telephone'),
    path('activate/<str:uidb64>/<str:token>', views.activation_view, name='activate'),
]
